const fs = require('fs');
const Lastfm = require('simple-lastfm');

async function scrobble(favorites) {
  let lastfm = new Lastfm({
    api_key: process.env.LASTFM_KEY,
    api_secret: process.env.LASTFM_SECRET,
    username: process.env.LASTFM_USER,
    password: process.env.LASTFM_PASS
  })

  lastfm.getSessionKey(async result => {
    if(!result.success) return console.log(`Error: ${result.error}`)

    await Promise.all(favorites.map(scrobbles))
    fs.writeFileSync(process.env.FAV_FILE, JSON.stringify(favorites))
  })

  function scrobbles (fav) {
    return new Promise(resolve => {     
      lastfm.scrobbleTrack({
        artist: fav.artist,
        track: fav.name,
        timestamp: fav.date,
        callback: () => {
          if (fav.likes < 3) return resolve()

          lastfm.loveTrack({
            artist: fav.artist, track: fav.name, callback: () => {
              resolve()
            }
          })
        }
      })
    })
  }
}

module.exports = { scrobble }