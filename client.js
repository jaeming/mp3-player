class Client {
  constructor(ws) { 
    this.ws = ws
    this.id = 0
  }

  send (method, params) {
    this.id += 1

    this.ws.send(JSON.stringify({
      jsonrpc: '2.0',
      method: `core.${method}`,
      params,
      id: this.id
    }))
    
    return this.id
  }

  getTracks () {
    const uri = 'local:directory?type=track'
    return this.send('library.browse', { uri })
  }

  addTracks (uris, at_position=0) {
    return this.send('tracklist.add', { uris, at_position })
  }

  play (tlid) {
    return this.send('playback.play', { tlid })
  }

  randomize () {
    return this.send('tracklist.set_random', [true])
  }

  getCurrentTrack () {
    return this.send('playback.get_current_track')
  }

  close () {
    this.ws.close()
  }
}

module.exports = { Client }
