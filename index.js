const WebSocket = require("ws")
const express = require('express')
const fs = require('fs')
const { Client } = require('./client')
const { scrobble } = require('./scrobbler')

let url = 'ws://localhost:6680/mopidy/ws/'
let client, getTracks, addTracklist, getCurrentSong, tracks

function connect () {
  const ws = new WebSocket(url)
  ws.on('open', onOpen)
  ws.on('message', onMessage)
  ws.on('error', onError)
  client = new Client(ws)
  let server = initServer(client)
}

function onOpen () {
  getTracks = client.getTracks() 
}

function onMessage (msg) {
  const { id, result } = JSON.parse(msg)
  if (!id) return

  if (getTracks === id) onGetTracks(result)
  if (addTracklist === id) onAddTrackList(result)
  if (getCurrentSong === id) onGetCurrentSong(result)
}

function onGetTracks (result) {
  tracks = result.map(i => i.uri)
  let random = tracks[~~(Math.random() * tracks.length)]
  addTracklist = client.addTracks([random])
}

function onAddTrackList (result) {
  if (!result) return
  client.randomize()
  client.play(result[0].tlid)
  client.addTracks(tracks, 1)
}

function onGetCurrentSong ({artists, name}) {
  let artist = artists[0].name
  let key = `${artist}: ${name}`
  let favorites = openFavorites()
  let date = Math.floor(Date.now() / 1000)
  let existing = favorites.find(i => i.key === key)
  existing
    ? Object.assign(existing, { likes: existing.likes + 1, date })
    : favorites.push({ key, artist, name, date, likes: 1 })
  fs.writeFileSync(process.env.FAV_FILE, JSON.stringify(favorites))
}

function onError (e) {
  if (e.code === 'ECONNREFUSED') {
    console.log('connection not ready...retrying in 5 secs')
    setTimeout(connect, 5000)
  }
}

function initServer() {
  server = express()
  server.get('/save', (_, res) => {
    getCurrentSong = client.getCurrentTrack()
    res.send('favorite requested')
  })
  server.get('/favorites', (_, res) => {
    let json = openFavorites()
    res.json(json)
  })
  server.get('/scrobble', (_, res) => {
    let favorites = openFavorites()
    scrobble(favorites)
    res.send('scrobbling...')
  })
  server.listen(5050, () => console.log('server listening at http://localhost:5050'))
}

function openFavorites() {
  return JSON.parse(fs.readFileSync(process.env.FAV_FILE))
}

connect()
